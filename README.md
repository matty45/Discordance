# WARNING: I AM NOT RESPONSIBLE FOR WHATEVER YOU DO WITH THIS CODE!!  STRICTLY FOR EDUCATIONAL PURPOSES!
# Discordance
A rat which uses Discord as a command and control center.
<br> Some features are designed for usage on CollabVM

![Stupid Logo](https://i.imgur.com/j8bG32q.png)

# Features
- Change wallpaper
- Create message boxes
- Take a screenshot
- Kill processes
- Disable and enable task manager
- Download and execute a file from the internet
- Basic persistence
- Delete files
- Block Input
- Get system information
- Uninstall rat


# To-Do
 - [ ] File browser?
 - [ ] Better stealth features.
 - [ ] Multiple clients.
 - [ ] Add full multi-threading support.
 - [ ] A builder?
 - [ ] Improve block input.
 - [ ] Obfuscate rat
 - [ ] Dont crash when connection lost and instead keep trying to connect
 - [ ] More persistence features

