﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using Microsoft.Win32;

// ReSharper disable TooManyArguments

namespace Discordance
{
    public sealed class Wallpaper
    {
        private Wallpaper()
        { }

        private const int SpiSetdeskwallpaper = 20;
        private const int SpifUpdateinifile = 0x01;
        private const int SpifSendwininichange = 0x02;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);

        public enum Style
        {
            Tiled,
            Centered,
            Stretched
        }

        // ReSharper disable once MethodTooLong
        // ReSharper disable once MethodNameNotMeaningful
        // ReSharper disable once FlagArgument
        public static void Set(Uri uri, Style style)
        {
            Stream s = new WebClient().OpenRead(uri.ToString());

            if (s != null)
            {
                Image img = Image.FromStream(s);
                string tempPath = Path.Combine(Path.GetTempPath(), "wallpaper.bmp");
                img.Save(tempPath, ImageFormat.Bmp);

                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);
                if (style == Style.Stretched)
                {
                    if (key != null)
                    {
                        key.SetValue(@"WallpaperStyle", 2.ToString());
                        key.SetValue(@"TileWallpaper", 0.ToString());
                    }
                }

                if (style == Style.Centered)
                {
                    if (key != null)
                    {
                        key.SetValue(@"WallpaperStyle", 1.ToString());
                        key.SetValue(@"TileWallpaper", 0.ToString());
                    }
                }

                if (style == Style.Tiled)
                {
                    if (key != null)
                    {
                        key.SetValue(@"WallpaperStyle", 1.ToString());
                        key.SetValue(@"TileWallpaper", 1.ToString());
                    }
                }

                SystemParametersInfo(SpiSetdeskwallpaper,
                    0,
                    tempPath,
                    SpifUpdateinifile | SpifSendwininichange);
            }
        }
    }
}