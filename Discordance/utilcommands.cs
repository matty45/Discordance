﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

// ReSharper disable UnusedMember.Global
// ReSharper disable ConditionIsAlwaysTrueOrFalse

namespace Discordance
{
    public class Utility : ModuleBase<SocketCommandContext>
    {

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetWindowTextLength(IntPtr hWnd);

        [Command("About")]
        [Summary("Gets some information about the bot.")]
        public async Task SysInfoRegAsync()
        {
            var builder = new EmbedBuilder()
                    .WithTitle("Discordance")
                    .WithDescription("A windows remote access tool which uses Discord as a command and control center. \n \n Use !help or @Discordance help for more info. \n \n Works with Windows 7 and up.")
                    .WithUrl("https://gitlab.com/matty45/Discordance")
                    .WithFooter(footer => {
                        footer
                            .WithText("Discordance created by Matthew#0712")
                            .WithIconUrl("https://cdn.discordapp.com/avatars/153565579146559488/ad5618067ab941e8905e3dbba7a6694f.png?");
                    })
                    .WithImageUrl("https://cdn.discordapp.com/avatars/447148087207788545/0d0b16f6298a9c888a7518a9e42cbc23.png");
            await Context.Channel.SendMessageAsync("", false, embed: builder.Build());
        }

        [Command("GetCurrentWindow")]
        [Summary("Gets some information about the window currently in focus.")]
        public async Task CurrentWindowInfoAsync()
        {
            IntPtr handle = GetForegroundWindow();
            var length = GetWindowTextLength(handle);
            StringBuilder sb = new StringBuilder(length + 1);
            StringBuilder lpClassName = new StringBuilder();
            GetClassName(handle, lpClassName, 100);
            GetWindowText(handle, sb, sb.Capacity);
            await ReplyAsync("Window Handle: " + handle);
            await ReplyAsync("Window Class Name: " + lpClassName);
            await ReplyAsync("Window Caption: " + sb);
        }
    }
}