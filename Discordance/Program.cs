﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net.Providers.WS4Net;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

namespace Discordance
{
    public class Program
    {
        public static CommandService Commands;
        private DiscordSocketClient _client;
        private IServiceProvider _services;
        public static char Botprefix = '!';

        private static void Main()
        {
            using (new System.Threading.Mutex(true, "DemoApp", out var createdNew))
            {
                if (createdNew)
                {
                    new Program().StartAsync().GetAwaiter().GetResult();
                }
                else
                {
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }
        }

        public async Task StartAsync()
        {
            _client = new DiscordSocketClient(new DiscordSocketConfig
            {
                // If your platform doesn't have native websockets,
                // add Discord.Net.Providers.WS4Net from NuGet,
                // add the `using` at the top, and uncomment this line:
                WebSocketProvider = WS4NetProvider.Instance
            });
            Commands = new CommandService();

            // Avoid hard coding your token. Use an external source instead in your code. | Dev Comment: How? durr..
            string token = "";

            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(Commands)
                .BuildServiceProvider();

            await InstallCommandsAsync();

            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();
            await _client.SetGameAsync("on " + Environment.MachineName);
            await Task.Delay(-1);
        }

        public async Task InstallCommandsAsync()
        {
            // Hook the MessageReceived Event into our Command Handler
            _client.MessageReceived += HandleCommandAsync;
            // Discover all of the commands in this assembly and load them.
            await Commands.AddModulesAsync(Assembly.GetEntryAssembly(), services: null);
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            // Don't process the command if it was a System Message
            var message = messageParam as SocketUserMessage;
            if (message == null) return;
            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;
            // Determine if the message is a command, based on if it starts with '!' or a mention prefix
            if (!(message.HasCharPrefix(Botprefix, ref argPos) || message.HasMentionPrefix(_client.CurrentUser, ref argPos))) return;
            // Create a Command Context
            var context = new SocketCommandContext(_client, message);
            // Disables reading commands from direct messages. Remove the bottom 2 lines if you dont want that.
            if (context.IsPrivate)
                return;
            // Execute the command. (result does not indicate a return value,
            // rather an object stating if the command executed successfully)
            var result = await Commands.ExecuteAsync(context, argPos, _services);
            if (!result.IsSuccess)
                await context.Channel.SendMessageAsync(result.ErrorReason);
        }
    }
}